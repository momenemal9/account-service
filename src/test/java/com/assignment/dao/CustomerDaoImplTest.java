package com.assignment.dao;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.dao.impl.CustomerDaoImpl;
import com.assignment.entity.Customer;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerDaoImplTest {

	@Mock
	private EntityManager entityManagerMock;

	@InjectMocks
	private CustomerDaoImpl customerDaoImpl;

	@Value("${query.get.customer.all}")
	private String getAllCustomersMockQuery;

	@Test
	public void getAllCustomersTest() {

		Query mockedQuery = Mockito.mock(Query.class);
		Mockito.when(entityManagerMock.createQuery(getAllCustomersMockQuery)).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.getResultList()).thenReturn(this.buildCustomerLst());

		List<Customer> custList = customerDaoImpl.getAllCustomers();

		assertNotNull(custList);
		Assert.assertEquals(custList.get(0), this.buildCustomerLst().get(0));
	}

	@Test
	public void getCustomerByIdTest() {

		Customer customer = new Customer();
		customer.setCustId(1l);
		Mockito.when(entityManagerMock.find(Customer.class, 1l)).thenReturn(customer);
		Customer customer2 = customerDaoImpl.getCustomerById(1l);
		assertNotNull(customer2);
	}


	private List<Customer> buildCustomerLst() {

		List<Customer> customerList = new ArrayList<>();
		Customer customer = new Customer();
		customer.setCustId(1l);
		// TODO Auto-generated method stub
		customerList.add(customer);
		return customerList;
	}
}
