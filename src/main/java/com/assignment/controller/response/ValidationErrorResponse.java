package com.assignment.controller.response;


import java.util.List;
import java.util.Map;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class ValidationErrorResponse {

    private String resultStatus;

    private String message;

    private Map<String, List<ErrorDetails>> errors;

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, List<ErrorDetails>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<ErrorDetails>> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ValidationErrorResponse [resultStatus=");
        builder.append(resultStatus);
        builder.append(", message=");
        builder.append(message);
        builder.append(", errors=");
        builder.append(errors);
        builder.append("]");
        return builder.toString();
    }

}
