/**
 * 
 */
package com.assignment.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class ResultMessage {

    @JsonProperty("resultCode")
    private String resultCode;

    @JsonProperty("resultStatus")
    private String resultStatus;

    @JsonProperty("message")
    private String message;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	@Override
	public String toString() {
		return "ResultMessage [resultCode=" + resultCode + ", resultStatus=" + resultStatus + ", message=" + message
				+ "]";
	}
    
    
}
