package com.assignment.exception;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class DuplicateCustomerException extends BaseException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @param errorCode
     * @param status
     * @param message
     */
    public DuplicateCustomerException(String errorCode, String status, String message) {
        super("DuplicateCustomerException", errorCode, status, message);
    }
}
