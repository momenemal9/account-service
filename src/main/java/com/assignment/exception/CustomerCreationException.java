package com.assignment.exception;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class CustomerCreationException extends BaseException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param exception
     * @param errorCode
     * @param status
     * @param message
     */
    public CustomerCreationException(String errorCode, String status, String message) {
        super("CustomerCreationException", errorCode, status, message);
    }

}
