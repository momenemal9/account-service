package com.assignment.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.assignment.dao.CustomerDao;
import com.assignment.entity.Customer;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@Transactional
@Repository
@PropertySource("classpath:sql_query.properties")
public class CustomerDaoImpl implements CustomerDao{

	
	@PersistenceContext
    private EntityManager entityManager;
	
	//Query to get all Customers.
	@Value("${query.get.customer.all}")
    private String queryGetAllCust;
	
	/** {@inheritDoc} */
	@Override
	public void addCustomer(Customer customer) {
		entityManager.persist(customer);
		
	}

	/** {@inheritDoc} */
	@Override
	public List<Customer> getAllCustomers() {
		return entityManager.createQuery(queryGetAllCust, Customer.class).getResultList();
		
	}

	/** {@inheritDoc} */
	@Override
	public Customer getCustomerById(Long customerId) {
		return entityManager.find(Customer.class, customerId);
		
	}

	
}
