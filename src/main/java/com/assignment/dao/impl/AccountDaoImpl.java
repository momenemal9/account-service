package com.assignment.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.assignment.dao.AccountDao;
import com.assignment.entity.Account;
import com.assignment.entity.Customer;
import com.assignment.util.Constants;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@Transactional
@Repository
@PropertySource("classpath:sql_query.properties")
public class AccountDaoImpl implements AccountDao {

	@PersistenceContext
	private EntityManager entityManager;

	// Query to get all Accounts.
	@Value("${query.get.account.all}")
	private String queryGetAllAccounts;

	// Query to get Account by Customer Id.
	@Value("${query.get.account.byCustomer}")
	private String queryGetAccountByCustomer;
	
	// Query to update Account's total balance.
		@Value("${query.update.account.totalBalance}")
		private String queryUpdateTotalBalance;

	/** {@inheritDoc} */
	@Override
	public Account createAccount(Account newAccount) {
		return entityManager.merge(newAccount);

	}

	/** {@inheritDoc} */
	@Override
	public List<Account> getAllAccounts() {
		return entityManager.createQuery(queryGetAllAccounts, Account.class).getResultList();
	}

	/** {@inheritDoc} */
	@Override
	public Account getAccountByCustomerId(Customer customer) {
		Account account = null;
		List<Account> accountList = entityManager.createQuery(queryGetAccountByCustomer, Account.class)
				.setParameter(Constants.CUSTOMER_ID, customer.getCustId()).getResultList();
		if (!accountList.isEmpty())
			account = accountList.get(0);
		return account;
	}

	/** {@inheritDoc} */
	@Override
	public void updateTotalBalance(Account existingAccount) {
		Query query = entityManager.createQuery(queryUpdateTotalBalance);
		query.setParameter(Constants.TOTAL_BALANCE, existingAccount.getTotalBalance());
		query.setParameter(Constants.ACCOUNT_ID, existingAccount.getAccntId());
		query.executeUpdate();
		
	}

}
