package com.assignment.dao;

import java.util.List;

import com.assignment.entity.Account;
import com.assignment.entity.Customer;

/**
 * This is Account DAO class
 * 
 * @author Dhiraj.Nangare
 *
 */
public interface AccountDao {

	/**
	 * Persist new Account into DB.
	 * 
	 * @param newAccount
	 */
	public Account createAccount(Account newAccount);

	/**
	 * Get all Accounts from DB .
	 * 
	 * @return
	 */
	public List<Account> getAllAccounts();

	/**
	 * Get Account By CustomerId from DB.
	 * 
	 * @param customer
	 * @return
	 */
	public Account getAccountByCustomerId(Customer customer);

	/**
	 * Update Account's Total balance
	 * 
	 * @param existingAccount
	 */
	public void updateTotalBalance(Account existingAccount);
}
