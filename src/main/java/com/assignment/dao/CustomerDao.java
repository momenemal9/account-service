package com.assignment.dao;

import java.util.List;

import com.assignment.entity.Customer;

/**
 * This is Customer DAO class
 * 
 * @author Dhiraj.Nangare
 *
 */
public interface CustomerDao {

	/**
	 * Persist new Customer into DB.
	 * 
	 * @param customer
	 */
	public void addCustomer(Customer customer);

	/**
	 * Get All Customers from DB.
	 * 
	 * @return List<Customer>
	 */
	public List<Customer> getAllCustomers();

	/**
	 * Get Customer By Id from DB.
	 * 
	 * @param customerId
	 * @return Customer
	 */
	public Customer getCustomerById(Long customerId);

}
