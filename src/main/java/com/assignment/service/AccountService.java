package com.assignment.service;

import java.util.List;

import com.assignment.controller.request.CustomerRequest;
import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.CustomerTransactionsResponse;

/**
 * Account service
 * 
 * @author Dhiraj.Nangare
 *
 */
public interface AccountService {

	/**
	 * This method is responsible to add new Customer to the system.
	 * 
	 * @param customerReq
	 * @return ResultMessage
	 */
	public ResultMessage addCustomer(CustomerRequest customerReq);

	/**
	 * This method is responsible to get all Customer and related Transactions.
	 * 
	 * @return List<CustomerTransactionsResponse>
	 */
	public List<CustomerTransactionsResponse> getAllCustomers();

	/**
	 * This method is responsible to create new Account and Transaction.
	 * 
	 * @param customerId
	 * @param initialCreditAmount
	 * @return ResultMessage
	 */
	public ResultMessage addInitialCredit(Long customerId, Long initialCreditAmount);

}
