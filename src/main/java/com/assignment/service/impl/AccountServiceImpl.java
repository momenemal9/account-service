package com.assignment.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.assignment.controller.request.CustomerRequest;
import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.CustomerTransactionsResponse;
import com.assignment.controller.response.TransactionResponseList;
import com.assignment.dao.AccountDao;
import com.assignment.dao.CustomerDao;
import com.assignment.entity.Account;
import com.assignment.entity.Customer;
import com.assignment.exception.CustomerCreationException;
import com.assignment.exception.DuplicateCustomerException;
import com.assignment.model.ResultCode;
import com.assignment.service.AccountService;
import com.assignment.util.Constants;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@Service
public class AccountServiceImpl implements AccountService {

	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private LoadBalancerClient loadBalancerClient;

	@Value("${pivotal.transactionService.name}")
	protected String transactionService;

	/** {@inheritDoc} */
	@Override
	public ResultMessage addCustomer(CustomerRequest customerReq) {
		ResultMessage resultMessage = new ResultMessage();
		// Build new Customer
		Customer customer = new Customer();
		customer.setFirstName(customerReq.getFirstName());
		customer.setLastName(customerReq.getLastName());
		customer.setEmailId(customerReq.getEmailId());
		try {
			// Persist into DB.
			customerDao.addCustomer(customer);
		} catch (PersistenceException ex) {
			Throwable e = ex.getCause();
            if (e instanceof ConstraintViolationException) {
            	logger.error("Customer already in use.");
                ResultCode result = ResultCode.DUPLICATE_CUSTOMER;
                throw new DuplicateCustomerException(result.getCode(), result.getStatus(), result.getMessage());
            }
            logger.error("Error while saving Customer. {} ", ex);
            ResultCode result = ResultCode.ADD_CUSTOMER_NOTIFICATION;
            throw new CustomerCreationException(result.getCode(), result.getStatus(), result.getMessage());
		}
		// Build success response.
		this.setResponseMessage(ResultCode.SUCCESS_ADD_CUSTOMER, resultMessage);
		return resultMessage;
	}

	/** {@inheritDoc} */
	@Override
	public List<CustomerTransactionsResponse> getAllCustomers() {

		List<CustomerTransactionsResponse> custTransList = new ArrayList<>();
		// Get Transaction-service instance using Load balancer client
		ServiceInstance serviceInstance=loadBalancerClient.choose(transactionService);
		URI transactionServiceUri=serviceInstance.getUri();
		// Get All customers from DB.
		List<Customer> custList = customerDao.getAllCustomers();

		for (Customer customer : custList) {
			// Build CustomerTransactionsResponse
			CustomerTransactionsResponse custTransResp = new CustomerTransactionsResponse();
			custTransResp.setCustId(customer.getCustId());
			custTransResp.setFirstName(customer.getFirstName());
			custTransResp.setLastName(customer.getLastName());
			custTransResp.setEmailId(customer.getEmailId());
			// Get linked Account with current Customer.
			Account existingAccount = accountDao.getAccountByCustomerId(customer);
			if (existingAccount != null) {
				// Call to Transaction-service to get associated Transactions with this account.
				ResponseEntity<TransactionResponseList> result = restTemplate.getForEntity(
						transactionServiceUri.toString() + "/transactions/{accountId}", TransactionResponseList.class,
						existingAccount.getAccntId());
				TransactionResponseList transactionResponseList = result.getBody();
				custTransResp.setAccountId(existingAccount.getAccntId());
				custTransResp.setAccountType(existingAccount.getAccountType());
				custTransResp.setTotalBalance(existingAccount.getTotalBalance());
				custTransResp.setTransactionList(transactionResponseList.getTransList());
			}
			custTransList.add(custTransResp);
		}
		return custTransList;
	}

	/** {@inheritDoc} */
	@Override
	public ResultMessage addInitialCredit(Long customerId, Long initialCreditAmount) {
		ResultMessage resultMessage = new ResultMessage();

		Long accntId = 0l;
		// Get Transaction-service instance using Load balancer client
		ServiceInstance serviceInstance=loadBalancerClient.choose(transactionService);
		URI transactionServiceUri=serviceInstance.getUri();
		// Get Customer dDetails from DB by Customer Id.
		Customer customer = customerDao.getCustomerById(customerId);
		if (customer != null) { // Customer is present
			// Get associated existing Account with this Customer.
			Account existingAccount = accountDao.getAccountByCustomerId(customer);
			if (existingAccount == null) {
				// Current Customer doesn't have any Account. Create new Current Account with initialCredit.
				Account newAccount = new Account();
				newAccount.setAccountType(Constants.CURRENT_ACCOUNT);
				newAccount.setTotalBalance(initialCreditAmount);
				newAccount.setCustomer(customer);
				Account accnt = accountDao.createAccount(newAccount);
				accntId = accnt.getAccntId();
			} else {
				// Account is already present. 
				// Get account Id of existing Account.
				accntId = existingAccount.getAccntId();
				if(initialCreditAmount > 0) {
					// Update Account's total balance.
					Long updatedBalance = existingAccount.getTotalBalance()+initialCreditAmount;
					existingAccount.setTotalBalance(updatedBalance);
					accountDao.updateTotalBalance(existingAccount);
				}
			}
			
			if(initialCreditAmount > 0) {
				// Call to Transaction-service to add new Transaction with this Account if Initial credit amount > 0.
				ResponseEntity<Double> result = restTemplate.getForEntity(
						transactionServiceUri.toString() + "/transaction/add/{accountId}/{amount}", Double.class, accntId,
						initialCreditAmount);
				if (result.getStatusCode() == HttpStatus.OK) {
					logger.info("Account transaction completed successfully");
					setResponseMessage(ResultCode.SUCCESS, resultMessage);
					return resultMessage;
				} else {
					logger.warn("AccountService:addInitialCredit -  HTTP not ok: ");
					setResponseMessage(ResultCode.TRANSACTION_FAILED, resultMessage);
					return resultMessage;
				}
			}
			
		} else {
			// Invalid Customer Id
			setResponseMessage(ResultCode.INVALID_CUSTOMER_ID, resultMessage);
			return resultMessage;
		}
		setResponseMessage(ResultCode.SUCCESS, resultMessage);
		return resultMessage;
	}

	public ResultMessage addInitialCreditFallBack(Long customerId, Long initialCreditAmount) {

		logger.error("AccountServiceImpl.addInitialCreditFallBack()===================");
		ResultMessage updateResponse = new ResultMessage();
		updateResponse.setResultStatus(ResultCode.TRANSACTION_FAILED.getStatus());
		updateResponse.setResultCode(ResultCode.TRANSACTION_FAILED.getCode());
		updateResponse.setMessage(ResultCode.TRANSACTION_FAILED.getMessage());
		return new ResultMessage();

	}
	
	
	public List<CustomerTransactionsResponse> getAllCustomersFallBack(Long customerId, Long initialCreditAmount) {

		logger.error("AccountServiceImpl.addInitialCreditFallBack()===================");
		return new ArrayList<CustomerTransactionsResponse>();

	}
	
	
	/**
	 * This method is used to build Response message.
	 * 
	 * @param updateResponse
	 */
	private void setResponseMessage(ResultCode resultCode, ResultMessage updateResponse) {
		updateResponse.setResultStatus(resultCode.getStatus());
		updateResponse.setResultCode(resultCode.getCode());
		updateResponse.setMessage(resultCode.getMessage());
	}

}
